<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ asset('css/app.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">jQuery, Bootstrap Fileinput Example</h1><br>
            <form enctype="multipart/form-data" method="POST" action="{{ route('products.store') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="file-loading">
                        <input id="image-file" name="image_file[]" class="file" type="file" multiple accept="image/*">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-outline-secondary">Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
    $("#image-file").fileinput({
        //language: 'es',
        browseOnZoneClick: true,
        dropZoneClickTitle: '<br>(or click to select images)',
        showUpload: false,
        //showCaption: true,
        showClose: false,
        msgPlaceholder: "Select images for upload...",
        maxFileCount: 6,
        allowedFileExtensions: ['jpg', 'png', 'bmp', 'jpeg', 'gif'],
        maxFileSize: 2048,
        // uploadUrl: "#",
        // fileActionSettings: {
        //     showRemove: true,
        //     showUpload: false,
        //     showZoom: true,
        //     showDrag: false,
        // },
    });
</script>
</body>
</html>